<?php

namespace DavidCsejteiWPLib\UI;

use DavidCsejteiWPLib\AbstractSingleton;

class AdminForms extends AbstractSingleton
{
    /**
     * Get current plugin directory URL.
     *
     * Get the URL of the current plugin's root directory.
     *
     * @since 0.2.0
     */
    public function addEmptyRow()
    {
        ?>
        <tr>
            <th></th>
        </tr>
        <?php
    }

    /**
     * Must be set 'file_uploads = On' in the php.ini to allow file uploads
     *
     * @param $fieldName
     * @param $label
     * @param bool $required
     */
    public function addFormBrowseFileField($fieldName, $label, $required = false)
    {
        ?>
        <tr>
            <th scope="row">
                <label for="<?= $fieldName ?>"><?= $label ?></label>
            </th>
            <td>
                <input id="file_browser" type='file' name='<?= $fieldName ?>'
                <?= ($required ? 'required>' : '>'); ?>
            </td>
        </tr>
        <?php
    }

    public function addFormSectionTitle($title)
    {
        ?>
        <tr>
            <th style="margin: 0; padding: 0" scope="row">
                <p style="font-size: 1.5em; font-weight: normal; margin: 0; padding: 0"><?= $title ?></p>
                <hr/>
            </th>
        </tr>
        <?php
    }

    public function addFormFieldAndLabel($fieldName, $label, $type, $defaultValue, $required = false)
    {
        ?>
        <tr>
            <th scope="row">
                <label for="<?= $fieldName ?>"><?= $label ?></label>
            </th>
            <td>
                <input name="<?= $fieldName ?>" type="<?= $type ?>" id="<?= $fieldName ?>" value="<?= $defaultValue ?>"
                       class="regular-text"
                <?= ($required ? 'required>' : '>'); ?>
            </td>
        </tr>
        <?php
    }

    public function addSubmitButton($label, $id)
    {
        ?>
        <tr>
            <th scope="row"></th>
            <td>
                <button class="button button-primary button-large" id="<?= $id ?>"><?= $label ?></button>
            </td>
        </tr>
        <?php
    }

    public function addCheckboxField($fieldName, $label, $type, $defaultValue, $required = false, $description)
    {
        ?>
        <tr>
            <th scope="row">
                <label for="<?= $fieldName ?>"><?= $label ?></label>
            </th>
            <td>
                <input name="<?= $fieldName ?>" type="<?= $type ?>" id="<?= $fieldName ?>" value="<?= $defaultValue ?>"
                       class="checkbox"
                <?= ($required ? 'required>' : '>'); ?>
                <p class="description"><?= $description ?></p>
            </td>
        </tr>
        <?php
    }

    function addSelectField($fieldLabel, $fieldName, $statuses, $actualStatus)
    {
        ?>
        <tr>
            <th scope="row">
                <label for="<?= $fieldName ?>"><?= $fieldLabel ?></label>
            </th>
            <td>
                <select name="<?= $fieldName ?>" id="<?= $fieldName ?>">
                    <?php
                    foreach ($statuses as $status) {
                        $a = '<option value="' . $status->getId() . '"';
                        $a .= $actualStatus == $status->getStatusLabel() ? 'selected' : '';
                        $a .= '>';
                        $a .= $status->getStatusLabel();
                        $a .= '</option>';
                        echo $a;
                    }
                    ?>
                </select>
            </td>
        </tr>
        <?php
    }

    public function addSpinner($id)
    {
        ?>
        <div id="<?= $id ?>" class="spinner"
             style="visiblity: hidden; display:none; float: left; width: auto; height: auto; padding: 10px 0 10px 20px; background-position: 0;"></div>
        <?php
    }

    public function addTextarea($fieldLabel, $fieldName, $value) {
        ?>
        <textarea name="<?= $fieldName ?>" id="<?= $fieldName ?>" rows="5" cols="30" class="widefat"><?= $value ?></textarea>
        <?php
    }
}
