# davidcsejtei-wp-lib
davidcsejtei-wp-lib is a library for WordPress plugin development.


## How to install
Use Composer:
```
#!bash
composer require davidcsejtei/davidcsejtei-wp-lib

```

## How to use
* [Examples](https://bitbucket.org/DavidCsejtei/davidcsejtei-wp-lib/wiki/Examples)

## Release notes
The release notes of the library is [here](https://bitbucket.org/DavidCsejtei/davidcsejtei-wp-lib/wiki/Release%20notes).

## Bugs

Create a bug report if you find any issue [here](https://bitbucket.org/DavidCsejtei/davidcsejtei-wp-lib/issues?status=new&status=open). 

## Packagist

You can find the davidcsejtei-wp-lib Packagist page [here](https://packagist.org/packages/davidcsejtei/davidcsejtei-wp-lib).