<?php

namespace DavidCsejteiWPLib\FileGenerator;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\TemplateProcessor;
use DavidCsejteiWPLib\AbstractSingleton;

class FileGenerator extends AbstractSingleton
{
    private $phpWord;
    private $template;

    const WORD = 'word';
    const PDF = 'pdf';
    const HTML = 'html';

    protected function __construct(array $metaData)
    {
        parent::__construct($metaData);

        $this->phpWord = new PhpWord();

        if($this->getTargetDir()) {
            //var_dump($this->getFullPathTemplateFileName());
            $this->template = new TemplateProcessor($this->getFullPathTemplateFileName());
        }
    }

    public function addSection($text)
    {
        $section = $this->phpWord->addSection();
        $section->addText($text);
    }

    public function setTemplateFile($templateFile) {
        $this->metaData['templateFile'] = $templateFile;
    }

    public function getTemplateFile() {
        return $this->metaData['templateFile'];
    }

    public function getFileName()
    {
        return $this->metaData['fileName'];
    }

    public function setFileName($fileName)
    {
        $this->metaData['fileName'] = $fileName;
    }

    public function getFileType()
    {
        return $this->metaData['fileType'];
    }

    public function setFileType($fileType)
    {
        $this->metaData['fileType'] = $fileType;
    }

    public function getFileExtension()
    {
        switch ($this->getFileType()) {
            case self::WORD:
                return "docx";
            case self::PDF:
                return "pdf";
            case self::HTML:
                return "html";
        }
    }

    public function getTargetDir()
    {
        return $this->metaData['targetDir'];
    }

    public function getFullPathTargetFileName()
    {
        return $this->getTargetDir() . "/" . "{$this->getFileName()}.{$this->getFileExtension()}";
    }

    public function getFullPathTemplateFileName()
    {
        return $this->getSourceDir() . "/" . "{$this->getTemplateFile()}.{$this->getFileExtension()}";
    }

    private function getPDFRendererName()
    {
        return Settings::PDF_RENDERER_TCPDF;
    }

    private function getRendererLibraryFileName()
    {
        return 'tcpdf.php';
    }

    private function getRendererLibraryPath()
    {
        $rendererLibrary = $this->getRendererLibraryFileName();
        return $this->metaData['rootDir'] . '/vendor/tecnickcom/tcpdf/' . $rendererLibrary;
    }

    private function setPDFRenderer()
    {
        Settings::setPdfRenderer($this->getPDFRendererName(), $this->getRendererLibraryPath());
    }

    public function save()
    {
        switch ($this->getFileType()) {
            case self::WORD:
                return $this->saveWord();
            case self::PDF:
                return $this->savePDF();
            case self::HTML:
                return $this->saveHTML();
        }
    }

    public function getFullPathSourceFileName()
    {
        return $this->getSourceDir() . $this->getSourceFile();
    }

    public function setSourceFile($sourceFile)
    {
        $this->metaData['sourceFile'] = $sourceFile;
    }

    public function getSourceFile()
    {
        return $this->metaData['sourceFile'];
    }

    public function getSourceDir()
    {
        return $this->metaData['sourceDir'];
    }

    public function setSourceDir($sourceDir)
    {
        $this->metaData['sourceDir'] = $sourceDir;
    }

    private function isSuccess()
    {
        return file_exists($this->getFullPathTargetFileName());
    }

    public function setValue($name, $value) {
        $this->template->setValue($name, $value);
    }

    public function saveWord()
    {
        if($this->template) {
            $this->template->saveAs($this->getFullPathTargetFileName());
        } else {
            $this->phpWord->save($this->getFullPathTargetFileName());
        }

        return $this->isSuccess();
    }

    public function savePDF()
    {
        $this->setPDFRenderer();
        $sourceFileContent = IOFactory::load($this->getFullPathSourceFileName());
        $pdfWriter = IOFactory::createWriter($sourceFileContent, 'PDF');
        $pdfWriter->save($this->getFullPathTargetFileName());
        return $this->isSuccess();
    }

    public function saveHTML()
    {
        $sourceFileContent = IOFactory::load($this->getFullPathSourceFileName());
        $xmlWriter = IOFactory::createWriter($sourceFileContent, 'HTML');
        $xmlWriter->save($this->getFullPathTargetFileName());
        return $this->isSuccess();
    }

}