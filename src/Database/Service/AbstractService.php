<?php

namespace DavidCsejteiWPLib\Database\Service;

use DavidCsejteiWPLib\AbstractSingleton;
use DavidCsejteiWPLib\Database\Model\AbstractModel;

abstract class AbstractService extends AbstractSingleton implements Service
{
    protected function __construct($metaData)
    {
        parent::__construct($metaData);
    }

    public function findBy($criteria, $type)
    {
        global $wpdb;

        $key = key($criteria);
        $criteria = array(
            'column' => $key,
            'value' => $criteria[$key]
        );

        $baseQuery = "SELECT * FROM " . $this->getTableName() . " WHERE " . $criteria["column"] . "=" . $this->getTypeTag($type);
        $query = $wpdb->prepare($baseQuery, $criteria["value"]);

        return $wpdb->get_row($query);
    }

    public function findMoreBy($criteria, $type) {
        global $wpdb;

        $key = key($criteria);
        $criteria = array(
            'column' => $key,
            'value' => $criteria[$key]
        );

        $baseQuery = "SELECT * FROM " . $this->getTableName() . " WHERE " . $criteria["column"] . "=" . $this->getTypeTag($type);
        $query = $wpdb->prepare($baseQuery, $criteria["value"]);

        return $wpdb->get_results($query);
    }

    public function getTableName()
    {
        return $this->metaData['tableName'];
    }

    function getAll()
    {
        global $wpdb;
        $query = "SELECT * FROM " . $this->getTableName();
        return $wpdb->get_results($query);
    }

    public function update($model, $criteria)
    {
        global $wpdb;

        $updated = $wpdb->update($this->metaData['tableName'], $model->getAsArray(), $criteria);

        if (!$updated && !empty($wpdb->last_error)) {
            throw new \DomainException($wpdb->last_error);
        } else {
            return $this->findBy($criteria, AbstractModel::NUMBER);
        }
    }

    public function add($model, $types)
    {
        global $wpdb;

        $inserted = $wpdb->insert($this->metaData['tableName'], $model, $this->getTypeTags($types));

        if (!$inserted) {
            if (empty($wpdb->last_error)) {
                throw new \DomainException("The cannot be inserted!");
            } else {
                throw new \DomainException($wpdb->last_error);
            }
        } else {
            return $this->findBy(array("ID" => $wpdb->insert_id), AbstractModel::NUMBER);
        }
    }

    private function getTypeTags($types)
    {
        $types = array();
        foreach ($types as $type) {
            $types[] = $this->getTypeTag($type);
        }
    }

    public function delete($criteria, $type)
    {
        global $wpdb;

        $model = $this->findBy($criteria, $type);

        $key = key($criteria);
        $criteria = array(
            'column' => $key,
            'value' => $criteria[$key]
        );

        $deleted = $wpdb->delete($this->metaData['tableName'], array($criteria["column"] => $criteria["value"]), array($this->getTypeTag($type)));

        if (!$deleted) {
            if (empty($wpdb->last_error)) {
                throw new \DomainException("The data cannot deleted!");
            } else {
                throw new \DomainException($wpdb->last_error);
            }
        } else {
            return $model;
        }
    }

    private function getTypeTag($type)
    {
        switch ($type) {
            case AbstractModel::NUMBER:
                return AbstractModel::NUMBER;
            case AbstractModel::TEXT:
                return AbstractModel::NUMBER;
        }
    }
}