<?php

namespace DavidCsejteiWPLib\Database\Service;

interface Service
{
    public function initDatabase();
    public function cleanDatabase();

    public function getAll();
    public function findBy($criteria, $type);

    public function add($model, $types);
    public function update($model, $criteria);
    public function delete($criteria, $type);
}

