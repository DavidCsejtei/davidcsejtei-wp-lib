<?php

namespace DavidCsejteiWPLib\Database\Model;

abstract class AbstractModel
{
    const NUMBER = '%d';
    const TEXT = '%s';

    public abstract function getAsArray();

    public static function createFromResult($result)
    {
    }
}