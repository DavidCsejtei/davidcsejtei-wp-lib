<?php

namespace DavidCsejteiWPLib;

abstract class AbstractSingleton
{
    static protected $instances = array();

    protected $metaData;

    public static function getInstance($metaData)
    {
        $class = get_called_class();
        if (!array_key_exists($class, self::$instances)) {
            self::$instances[$class] = new $class($metaData);
        }
        return self::$instances[$class];
    }

    protected function __construct($metaData = array())
    {
        $this->metaData = $metaData;
    }

    private function __clone()
    {
    }
    
    private function __wakeup()
    {
    }
}