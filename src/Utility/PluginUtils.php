<?php

namespace DavidCsejteiWPLib\Utility;

use DavidCsejteiWPLib\AbstractSingleton;

class PluginUtils extends AbstractSingleton
{
    /**
     * Create ID for a page
     *
     * @since 0.2.0
     * @param String label of the page
     * @return String page ID
     */
    public function getUniquePageID($label)
    {
        return $this->getUniqueID($label, 'page');
    }

    /**
     * Create ID for a sub page
     *
     * @since 0.2.0
     * @param String label of the sub page
     * @return String sub page ID
     */
    public function getUniqueSubPageID($label)
    {
        return $this->getUniqueID($label, 'subpage');
    }

    /**
     * Create ID for a script enqueued
     *
     * @since 0.2.0
     * @param String label of the sub page
     * @return String sub page ID
     */
    public function getUniqueScriptID($label)
    {
        return $this->getUniqueID($label, 'script');
    }

    public function getUniqueStyleID($label)
    {
        return $this->getUniqueID($label, 'style');
    }

    /**
     * Create ID for different type of element
     *
     * Create ID for different type of element, eg.: page, subpage, enque script, etc.
     *
     * @since 0.2.0
     * @return String URL.
     */
    private function getUniqueID($label, $type)
    {
        switch ($type) {
            case 'page':
                return $this->metaData['plugin-slug'] . '-' . $label . '-page';
            case 'script':
                return $this->metaData['plugin-slug'] . '-' . $label . '-script';
            case 'style':
                return $this->metaData['plugin-slug'] . '-' . $label . '-style';
            case 'subpage':
                return $this->metaData['plugin-slug'] . '-' . $label . '-subpage';
        }
    }

    public function getPluginSlug() {
        return $this->metaData['plugin-slug'];
    }

    /**
     * Get current plugin directory URL.
     *
     * Get the URL of the current plugin's root directory.
     *
     * @since 0.1.0
     * @return String URL.
     */
    public function getBaseURL($file)
    {
        return plugin_dir_url(dirname($file)) . $this->metaData['plugin-slug'] . "/";
    }

    /**
     * Calculate left time with deadline start date and deadline length.
     *
     * @param $from - Start date of the deadline length
     * @param $deadlineLength - Length of the deadline
     * @return Date - End of the deadline
     */
    public function calculateLeftTimeFromDeadline($from, $deadlineLength)
    {
        $from = strtotime($from);

        switch ($deadlineLength) {
            case "12 óra":
                $deadlineLength = "12 hours";
                break;
            case "24 óra":
                $deadlineLength = "24 hours";
                break;
            case "1 hónap":
                $deadlineLength = "1 months";
                break;
            case "2 hónap":
                $deadlineLength = "2 months";
                break;
            case "6 hónap":
                $deadlineLength = "6 months";
                break;
        }

        $deadline = strtotime($deadlineLength, $from);

        return date("Y.m.d. H:i", $deadline);
    }

    public function userIsACustomer()
    {
        return $this->hasUserRole('megrendelo');
    }

    public function userIsAnOperator()
    {
        return $this->hasUserRole('ugyintezo');
    }

    public function hasUserRole($role)
    {
        $user = \wp_get_current_user();
        return in_array($role, $user->roles);
    }

    public function getPluginFolder()
    {
        $dir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
        return plugin_dir_url($dir);
    }

    public function getCurrentUserEmail() {
        $user = get_currentuserinfo();
        return $user->user_email;
    }
}