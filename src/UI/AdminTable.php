<?php

namespace DavidCsejteiWPLib\UI;

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class AdminTable extends \WP_List_Table
{
    public function __construct()
    {
        parent::__construct();
    }

    public function prepare_items() {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
    }

    protected function order_items($data) {
        usort($data, array($this, 'orderTable'));

        $perPage = 100;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);

        $this->set_pagination_args(array(
            'total_items' => $totalItems,
            'per_page' => $perPage
        ));

        return array_slice($data, (($currentPage - 1) * $perPage), $perPage);
    }

    private function orderTable($a, $b)
    {
        $orderby = (!empty($_GET['orderby'])) ? $_GET['orderby'] : 'upload_time';
        $order = (!empty($_GET['order'])) ? $_GET['order'] : 'desc';
        $result = strnatcmp($a[$orderby], $b[$orderby]);
        return ($order === 'asc') ? $result : -$result;
    }
}